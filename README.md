# Force Login Redirect

Forcing a redirect to the WordPress login page for users that are not logged in.